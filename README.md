```
        ___  _         ___   ___  _   _       _     _             
 _ __  / _ \| |_ _ __ / _ \ / _ \| |_( )___  | |__ | | ___   __ _ 
| '_ \| | | | __| '__| | | | | | | __|// __| | '_ \| |/ _ \ / _` |
| | | | |_| | |_| |  | |_| | |_| | |_  \__ \ | |_) | | (_) | (_| |
|_| |_|\___/ \__|_|   \___/ \___/ \__| |___/ |_.__/|_|\___/ \__, |
                                                            |___/ 
```

# Status  
[![pipeline status](https://gitlab.com/n0tr00teuorg/n0tr00teuorg.gitlab.io/badges/main/pipeline.svg)](https://gitlab.com/n0tr00teuorg/n0tr00teuorg.gitlab.io/-/commits/main)
[![buddy pipeline](https://app.buddy.works/n0tr00t/n0tr00t/pipelines/pipeline/402630/badge.svg?token=6c877a444a44b23dc585b652daf9d20c8f69884787282075617637dd83de3f4b "buddy pipeline")](https://app.buddy.works/n0tr00t/n0tr00t/pipelines/pipeline/402630)
[![Netlify Status](https://api.netlify.com/api/v1/badges/979f4af0-79d0-47db-9b9f-8af8aa89d960/deploy-status)](https://app.netlify.com/sites/n0tr00t/deploys)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=n0tr00teuorg_n0tr00teuorg.github.io&metric=alert_status)](https://sonarcloud.io/summary/new_code?id=n0tr00teuorg_n0tr00teuorg.github.io)
# ToDo  
- [x] 域名
	- [x] eu.org
	- [ ] ~~js.org~~
	- [ ] Tor
- [x] 代码托管
	- [ ] ~~Gitee~~
	- [ ] Gitea
	- [x] GitLab
	- [x] GitHub
- [x] 自动部署
	- [x] Buddy
	- [x] netlify
	- [ ] ...
- [ ] 评论
	- [ ] Valine
- [ ] 内容交付网络
	- [ ] ...
- [ ] 爬虫
	- [x] Google
	- [x] Yandex
	- [x] Bing
	- [ ] Yahoo
	- [ ] Baidu
	- [ ] 360
	- [ ] Sogou
	- [ ] ...
